# SilexPvP | Ultra Hardcore

Base Ultra Hardcore addon for the Silex Network (silexpvp.net)

## Getting Started

 1. Download and setup and IDE.
 2. Download and setup maven.
 3. Import the project into your local machine.
 4. Make changes and build the project.

### Prerequisites

What things you need to install the software and how to install them.

```
 - Integrated Development Environment
 - Java SDK and JDK (8, 9 or 10)
 - Maven
 - A brain
```

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Authors

* **Álvaro "JavaNANA" ** - *Founder and main developer* - [JavaNANA](https://bitbucket.org/JavaNANANA/)

## License

This project is licensed under the Apache 2 License (https://directory.fsf.org/wiki/License:Apache2.0) - (Be careful on how to use this source and read the license)

## Contributions

* Thanks to "Handsito" for helping me with some stuff.
* Thanks to (https://twitter.com/UltraGameFeed) for the inspiration.
